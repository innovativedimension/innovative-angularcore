# BIT Commands
## Adding files to the component
* **bit init** - Initialize the component
* **bit add <directory> --main module** - add the component files. Usually a module or lib.
* **bit status** - Check the component status and missing dependencies.
* **bit install** - Install missing dependencies.
* **bit import bit.envs/compilers/angular --compiler** - Add one Angular compiler.
* **bit build** - Build the component and prepare for publication.
* **bit tag --all 0.1.0** - Tag the component to a release point.
* **bit export <collection>** - Export the latest component tags to the BIT repository.
